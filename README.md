### Test Weather Project (Java Spring)

<strong>Description</br></strong>
Weather API that given the name of a city( city) returns the temperature for the current time.

<strong>@GetMapping("/api/woeid")</strong>

Get is used as a method for the request because data persistence is not required and the name of the city doesn’t represent any PII (Personally Identifiable Information). Data is sent (city's name) with @RequestParam. It will always expect a value to bind and it will avoid an error. Also @RequestParam adds a security layer if the key is modified.</br>
For example request: http://localhost:8080/api/woeid/?city=Los Angeles

<strong>Response</strong>

JSON with the information in order; City, Celsius and Fahrenheit.

For example

{

    "city": "Los Angeles",
    
    "celsius": 25.49,
    
    "farenheit": 77.882
    
}

