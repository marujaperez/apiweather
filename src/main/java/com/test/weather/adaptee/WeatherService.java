package com.test.weather.adaptee;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.test.weather.dto.ConsolidatedWeatherDTO;
import com.test.weather.dto.WoeidDTO;
import com.test.weather.exception.InternalServerErrorException;

@Service
public class WeatherService {
	@Value("${api.weather.protocol}")
	private String apiWeatherProtocol;

	@Value("${api.weather.host}")
	private String apiWeatherHost;

	@Value("${api.weather.path}")
	private String apiWeatherPath;

	@Value("${api.weather.path.search}")
	private String apiWeatherSearch;

	@Autowired
	private RestTemplate restTemplate;

	/*
	 * 
	 */
	public List<WoeidDTO> getWoeid(String city) {

		List<WoeidDTO> woeid = new ArrayList<WoeidDTO>();

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

			UriComponents uriComponents = UriComponentsBuilder.newInstance().scheme(apiWeatherProtocol)
					.host(apiWeatherHost).path(apiWeatherPath).path(apiWeatherSearch).queryParam("query", city)
					.build(false);
			URI uri = uriComponents.toUri();

			HttpEntity<?> entity = new HttpEntity<>(headers);
			ResponseEntity<String> responseW = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

			Integer success = responseW.getStatusCodeValue();

			if (success.equals(200)) {
				JSONArray jsonresp = new JSONArray(responseW.getBody());
				Gson gson = new Gson();
				Type type = new TypeToken<List<WoeidDTO>>() {
				}.getType();
				woeid = gson.fromJson(jsonresp.toString(), type);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException("Error processing information");
		}

		return woeid;
	}

	/*
	 * 
	 */

	public List<ConsolidatedWeatherDTO> getConsolidatedWeather(Long woeid) {

		List<ConsolidatedWeatherDTO> consolidatedWeather = new ArrayList<ConsolidatedWeatherDTO>();

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

			UriComponents uriComponents = UriComponentsBuilder.newInstance().scheme(apiWeatherProtocol)
					.host(apiWeatherHost).path(apiWeatherPath).path(woeid.toString()).build(false);
			URI uri = uriComponents.toUri();

			HttpEntity<?> entity = new HttpEntity<>(headers);

			ResponseEntity<String> responseCW = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

			Integer success = responseCW.getStatusCodeValue();

			if (success.equals(200)) {
				JSONObject jsonresp = new JSONObject(responseCW.getBody());
				JSONArray jsonConsolidatedWeather = jsonresp.getJSONArray("consolidated_weather");
				Gson gson = new Gson();
				Type type = new TypeToken<List<ConsolidatedWeatherDTO>>() {
				}.getType();
				consolidatedWeather = gson.fromJson(jsonConsolidatedWeather.toString(), type);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerErrorException("Error processing information");
		}

		return consolidatedWeather;
	}

}
