package com.test.weather.adapter;

import static com.test.weather.utils.BaseValidations.convertTemperature;
import static com.test.weather.utils.BaseValidations.validateNullEmpty;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.test.weather.adaptee.WeatherService;
import com.test.weather.dto.ConsolidatedWeatherDTO;
import com.test.weather.dto.TemperatureDTO;
import com.test.weather.dto.WoeidDTO;
import com.test.weather.target.TemperatureAdapter;

@Component
public class TemperatureAdapterImpl implements TemperatureAdapter {

	@Autowired
	private WeatherService weatherService;

	@Override
	/*
	 * 
	 */
	public ResponseEntity<?> getTemperature(String city) {
		if (validateNullEmpty(city)) {
			return new ResponseEntity<>("Please, you should type a city.", HttpStatus.OK);
		}
		List<WoeidDTO> woeidDTO = weatherService.getWoeid(city.trim());
		Long woeidResponse = 0L;
		TemperatureDTO temperatureDTO;
		if (woeidDTO != null) {
			if (woeidDTO.size() == 1 && woeidDTO.get(0).getTitle().equalsIgnoreCase(city.trim())) {
				woeidResponse = woeidDTO.get(0).getWoeid();
				temperatureDTO = new TemperatureDTO();
				temperatureDTO.setCity(woeidDTO.get(0).getTitle());
				temperatureDTO.setCelsius(getTemp(weatherService.getConsolidatedWeather(woeidResponse)));
				temperatureDTO.setFarenheit(convertTemperature(temperatureDTO.getCelsius()));
				return new ResponseEntity<TemperatureDTO>(temperatureDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Please, detail the search by city.", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

	/*
	 * 
	 */
	private Double getTemp(List<ConsolidatedWeatherDTO> consolidatedWeatherDTO) {
		ConsolidatedWeatherDTO consolidate = new ConsolidatedWeatherDTO();
		if (consolidatedWeatherDTO != null) {
			int index = consolidatedWeatherDTO.size();
			consolidate = consolidatedWeatherDTO.get(index - 1);
		}
		return consolidate.getThe_temp();
	}
}
