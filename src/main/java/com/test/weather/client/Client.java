package com.test.weather.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.test.weather.adapter.TemperatureAdapterImpl;

@RestController
public class Client {
	private TemperatureAdapterImpl temperatureAdapterImpl;

	@Autowired
	public Client(TemperatureAdapterImpl temperatureAdapterImpl) {
		this.temperatureAdapterImpl = temperatureAdapterImpl;
	}

	@GetMapping("/api/woeid")
	public ResponseEntity<?> getTemperature(@RequestParam String city) {
		return new ResponseEntity<>(this.temperatureAdapterImpl.getTemperature(city), HttpStatus.OK).getBody();
	}
}
