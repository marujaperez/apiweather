package com.test.weather.dto;

import lombok.Data;

@Data
public class ConsolidatedWeatherDTO {

	private Long id;
	private String weatherStateName;
	private String weatherStateAbbr;
	private String windDirectionCompass;
	private String created;
	private String applicableDate;
	private Double min_temp;
	private Double max_temp;
	private Double the_temp;
	private Double wind_speed;
	private Double wind_direction;
	private Double air_pressure;
	private Integer humidity;
	private Double visibility;
	private Integer predictability;
}
