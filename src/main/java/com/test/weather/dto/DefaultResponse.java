package com.test.weather.dto;

import lombok.Data;

@Data
public class DefaultResponse {
	private int codigo;
	private String mensaje;

	public DefaultResponse(int codigo, String mensaje) {
		this.codigo = codigo;
		this.mensaje = mensaje;
	}
}
