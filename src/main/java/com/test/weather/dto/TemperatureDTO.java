package com.test.weather.dto;

import lombok.Data;

@Data
public class TemperatureDTO {

	private String city;

	private Double celsius;

	private Double farenheit;

}
