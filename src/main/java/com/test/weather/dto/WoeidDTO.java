package com.test.weather.dto;

import lombok.Data;

@Data
public class WoeidDTO {
	private String title;
	private String location_type;
	private Long woeid;
	private String latt_long;

}
