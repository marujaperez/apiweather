package com.test.weather.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

@Getter
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/*
	 * 
	 */
	public InternalServerErrorException() {
		super();
	}

	/*
	 * 
	 */
	public InternalServerErrorException(String message) {
		super(message);
	}

}
