package com.test.weather.target;

import org.springframework.http.ResponseEntity;

public interface TemperatureAdapter {

	ResponseEntity<?> getTemperature(String city);
}
