package com.test.weather.utils;

import org.springframework.context.annotation.Bean;

public abstract class BaseValidations {

	/*
	 * 
	 */
	@Bean
	public static double convertTemperature(Double celsius) {
		return (9.0 / 5.0) * celsius + 32;
	}

	/*
	 * 
	 */
	@Bean
	public static boolean validateNullEmpty(String city) {
		return city == null || city.isEmpty();
	}

}
